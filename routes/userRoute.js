const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post("/logout", userController.logout);
router.post("/resetPassword", userController.resetPassword);
router.get(
  "/protected",
  userController.authenticateToken,
  userController.getProtected
);
router.get("/tes", userController.test);
router.post("/webhook", userController.webhook);

module.exports = router;
