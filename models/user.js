const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  username: String,
  password: String,
  phone: String,
  email: String,
});

const User = mongoose.model("students", userSchema);
module.exports = User;
