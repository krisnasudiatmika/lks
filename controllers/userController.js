const User = require("../models/user.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

let tokenBlacklist = [];

const keySecret = process.env.JWT_SECRET;

exports.register = async (req, res) => {
  try {
    const { username, password, email, phone } = req.body;
    if (!(username && password)) {
      return res.status(400).send("Username and password are required");
    }

    const existingUser = await User.findOne({ username });
    if (existingUser) {
      return res.status(400).send("User already exists");
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new User({
      username,
      password: hashedPassword,
      phone,
      email,
    });

    await user.save();
    res.status(201).send("User created");
  } catch (error) {
    console.error(error);
    res.status(500).send("Error registering new user");
  }
};

exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ username });
    if (!user || !(await bcrypt.compare(password, user.password))) {
      return res.status(401).send("Invalid credentials");
    }

    const token = jwt.sign({ username }, keySecret, {
      expiresIn: "1h",
    });

    res.status(200).json({ token });
  } catch (error) {
    console.error(error);
    res.status(500).send("Error logging in");
  }
};

exports.logout = (req, res) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token) {
    tokenBlacklist.push(token);
    res.status(200).send("Logged out successfully");
  } else {
    res.status(400).send("Token is required");
  }
};

exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const authHeader = req.headers["authorization"];
    const token = authHeader && authHeader.split(" ")[1];
    if (!token || !newPassword) {
      return res.status(400).send("Token and new password are required");
    }

    const decoded = jwt.verify(token, keySecret);
    const user = await User.findOne({ username: decoded.username });
    if (!user) {
      return res.status(400).send("User not found");
    }

    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();

    res.status(200).send("Password has been reset");
  } catch (error) {
    console.error(error);
    res.status(500).send("Error resetting password");
  }
};

exports.authenticateToken = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (!token) return res.status(401).send("Access Denied");

  if (tokenBlacklist.includes(token)) {
    return res.status(401).send("Token has been invalidated");
  }

  jwt.verify(token, keySecret, (err, user) => {
    if (err) return res.status(403).send("Invalid Token");
    req.user = user;
    next();
  });
};

exports.getProtected = (req, res) => {
  res.status(200).send(`Protected content accessed by ${req.user.username}`);
};

exports.test = (req, res) => {
  res.status(200).send("Welcome to the test endpoint");
};

exports.webhook = (req, res) => {
  console.log("Received webhook:", JSON.stringify(req.body, null, 2));
  res.status(200).send("Webhook data received");
};
